import React from 'react'
import ServerComponent from './ServerComponent'
import products from '@fixtures/products.json'
// Import style
import '../../app/globals.css'

describe('<ServerComponent />', () => {
  it('should render a server component', async () => {
    // Stub window.fetch
    cy.stub(window, 'fetch').resolves({
      json: cy.stub().resolves(products)
    })

    const component = await ServerComponent()
    // Mount the awaited server component
    cy.mount(component)

    // Basic mount doesn't work
    // cy.mount(<ServerComponent />)
  })
})
