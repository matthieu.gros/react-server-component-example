'use client';
import React, {PropsWithChildren, useState} from "react";
import ServerComponent from "@/app/components/ServerComponent";
import {NeutralComponent} from "@/app/components/NeutralComponent";

export const ClientComponent = (props: PropsWithChildren) => {
  const [count, setCount] = useState(0)

  const increaseCount = () => {
    setCount(count + 1)
  }

  return (
    <div className="border-solid rounded border-2 border-green-500 p-3 mb-4">
      <h2>Client component</h2>
      <button type="button" onClick={increaseCount}
        className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2 ">Ajouter
      </button>
      <p className="text-4xl font-semibold text-gray-900 dark:text-white">{ count }</p>
      { props.children }
    </div>
  )
}
