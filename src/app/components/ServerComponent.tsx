import React, {PropsWithChildren, Suspense} from "react";
import {ProductCardComponent} from "@/app/components/ProductCard";
export default async function ServerComponent() {
  const products: Array<{ title: string, description: string, image: string, id: number }> = await fetch('https://fakestoreapi.com/products')
    .then((response: Response) => response.json())

  await new Promise((resolve) => setTimeout(resolve, 3000))

  return(
      <div className="p-3 border-solid rounded border-2 border-indigo-600">
        <h2>Server component</h2>
        {products.map((product) =>
          <ProductCardComponent key={product.id} product={product} />
        )}
      </div>
  )
}
