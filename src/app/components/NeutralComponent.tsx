import {PropsWithChildren} from "react";

export const NeutralComponent = () => {
  const isClientComponent = typeof window !== 'undefined'

  return (
    <p className="text-lg text-gray-900 dark:text-white border-dashed border-2 border-indigo-600 p-2 mb-2">
      {`${isClientComponent ? 'client' : 'server'} component`}
    </p>
  )
}
