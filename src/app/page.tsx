import ServerComponent from "@/app/components/ServerComponent";
import {ClientComponent} from "@/app/components/ClientComponent";
import React, {Suspense} from "react";
import {ServerComponentPlaceholder} from "@/app/components/ServerComponentPlaceholder";
import {NeutralComponent} from "@/app/components/NeutralComponent";
export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <ClientComponent>
        <Suspense fallback={<ServerComponentPlaceholder />}>
          <ServerComponent />
        </Suspense>
      </ClientComponent>
    </main>
  )
}
