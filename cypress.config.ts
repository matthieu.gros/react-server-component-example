import { loadEnvConfig } from '@next/env';
import { defineConfig } from "cypress";

const { combinedEnv } = loadEnvConfig(process.cwd());export default defineConfig({
  env: combinedEnv,
  component: {
    viewportHeight: 1080,
    viewportWidth: 1920,
    devServer: {
      framework: "next",
      bundler: "webpack",
    },
  },
  e2e: {
    viewportHeight: 1080,
    viewportWidth: 1920,
    baseUrl: 'http://localhost:3000',
    retries: {
      runMode: 3,
    },
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
